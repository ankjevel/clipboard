//
//  MainWindow.swift
//  clipboard
//
//  Created by Dennis Pettersson on 2015-01-30.
//  Copyright (c) 2015 bldr.se. All rights reserved.
//

import Cocoa
import AppKit
import Foundation

class MainWindow: NSWindow {
  lazy
  var storyBoard: NSStoryboard = {
    return NSStoryboard(name: "Main", bundle: nil)!
  }()
  
  lazy
  var controller: ViewController = {
    return self.storyBoard.instantiateControllerWithIdentifier("viewController") as ViewController!
  }()
  
  func isMovableByWindowBackground() -> Bool {
    return true
  }
  
  override
  func awakeFromNib() {

//    let width = self.frame.width
//    let height = self.frame.height
//    let visualEffectView = NSVisualEffectView(frame: NSMakeRect(0, 0, width, height))
//    visualEffectView.material = NSVisualEffectMaterial.Dark
//    visualEffectView.blendingMode = NSVisualEffectBlendingMode.BehindWindow
////    visualEffectView.state = NSVisualEffectState.Active
////    self.contentView.addSubview(visualEffectView)
//    self.contentView.addSubview(visualEffectView, positioned: NSWindowOrderingMode.Above, relativeTo: controller.view)
//    self.contentView.addSubview(controller.view)
    
    self.styleMask = self.styleMask | NSFullSizeContentViewWindowMask
    self.titlebarAppearsTransparent = true    
    self.appearance = NSAppearance(named: NSAppearanceNameVibrantDark)
  }

//  func keyEvent(event: NSEvent) {
//    let type = event.type.rawValue
//    println("event: \(event)")
//  }
  
//  override func keyDown(theEvent: NSEvent) { keyEvent(theEvent) }
//  override func keyUp(theEvent: NSEvent) { keyEvent(theEvent) }
}