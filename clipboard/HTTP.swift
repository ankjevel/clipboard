//
//  HTTP.swift
//  clipboard
//
//  Created by Dennis Pettersson on 2015-02-06.
//  Copyright (c) 2015 bldr.se. All rights reserved.
//

import Cocoa
import Foundation

class HTTP: NSObject {
  
  typealias requestHandler = (responseData: NSData!, urlResponse: NSURLResponse!, error: NSError!) -> Void
  
  private
  func mutableRequest(url: String, method: String? = "POST") -> NSMutableURLRequest {
    var request = NSMutableURLRequest(URL: NSURL(string: url)!)
    
    request.addValue("multipart/form-data; boundary=\(boundary)",
      forHTTPHeaderField: "Content-Type")
    request.addValue("localhost",
      forHTTPHeaderField: "Origin")
    
    request.HTTPMethod = method!.uppercaseString
    
    return request
  }
  
  private
  var boundary: String = "----------aA1bB2cC3dD4eE5fF6"
  
  private
  func createImageRequest(data: NSData) -> NSMutableURLRequest {
    var request = mutableRequest("http://api.bldr.se/2/")
    
    let img = NSImage(dataIgnoringOrientation: data)!
    let imageData = img.TIFFRepresentation!
    let imageRep = NSBitmapImageRep(data: imageData)!
    let representation =
    imageRep.representationUsingType(NSBitmapImageFileType.NSPNGFileType,
      properties: [:])!
    
    var body = NSMutableData()
    body.appendData("--\(boundary)\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
    body.appendData("Content-Disposition: form-data; name=\"image\"; filename=\"blob\"\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
    body.appendData("Content-Type: image/png\r\n\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
    body.appendData(representation)
    body.appendData("\r\n--\(boundary)\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
    body.appendData("Content-Disposition: form-data; name=\"pub\"\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
    body.appendData("\r\nZrEd2vAhAh+VIYq27kJk6qnCRtn_v_RMkm0Y381VsVTwLcxsCu".dataUsingEncoding(NSUTF8StringEncoding)!)
    body.appendData("\r\n--\(boundary)--\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
    
    request.HTTPBody = body
    
    return request
  }
  
  private
  func createDeleteRequest(imageHash: String, delHash: String) -> NSMutableURLRequest {
    var request = mutableRequest("http://bldr.se/delete/\(delHash)\(imageHash)")
    var body = NSMutableData()
    body.appendData("--\(boundary)\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
    body.appendData("Content-Disposition: form-data; name=\"has\"\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
    body.appendData("\r\n\(imageHash)".dataUsingEncoding(NSUTF8StringEncoding)!)
    body.appendData("\r\n--\(boundary)\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
    body.appendData("Content-Disposition: form-data; name=\"del\"\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
    body.appendData("\r\n\(delHash)".dataUsingEncoding(NSUTF8StringEncoding)!)
    body.appendData("\r\n--\(boundary)--\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
    
    request.HTTPBody = body
    return request
  }
  
  private
  func newTask(req: NSMutableURLRequest, onResponse: requestHandler) {
    var session = NSURLSession.sharedSession()
    var task =
    session.dataTaskWithRequest(req,
      completionHandler: onResponse)
    task.resume()
  }
  
  func request(data: NSData, handleCreateResponse: requestHandler) {
    newTask(createImageRequest(data), onResponse: handleCreateResponse)
  }
  
  func request(imageHash: String, delHash: String, handleDeleteResponse: requestHandler) {
    newTask(createDeleteRequest(imageHash, delHash: delHash), onResponse: handleDeleteResponse)
  }
}
