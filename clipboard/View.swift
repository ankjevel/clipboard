//
//  View.swift
//  clipboard
//
//  Created by Dennis Pettersson on 2015-02-04.
//  Copyright (c) 2015 bldr.se. All rights reserved.
//

import Cocoa

class View: NSView {
  
  override
  func drawRect(dirtyRect: NSRect) {
    super.drawRect(dirtyRect)
  }
  
  func mouseDownCanMoveWindow() -> Bool {
    return true
  }
  
  func viewDidLoad() {
    self.window?.titlebarAppearsTransparent = true
    self.window?.movableByWindowBackground  = true
  }

}
