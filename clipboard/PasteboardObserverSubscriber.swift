//
//  PasteboardObserverSubscriber.swift
//  clipboard
//
//  Created by Dennis Pettersson on 2015-01-31.
//  Copyright (c) 2015 bldr.se. All rights reserved.
//

import Foundation
import Cocoa

@objc protocol PasteboardObserverSubscriber: NSObjectProtocol {
  func pasteboardChanged(pasteboard: NSPasteboard)
}