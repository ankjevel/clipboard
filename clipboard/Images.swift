
//
//  Images.swift
//  clipboard
//
//  Created by Dennis Pettersson on 2015-02-04.
//  Copyright (c) 2015 bldr.se. All rights reserved.
//

import Cocoa
import Foundation
import CoreData

class Images:
  NSObject {

  var http = HTTP()

  lazy
  var appDelegate: AppDelegate = {
    let appDelegate =
    NSApplication.sharedApplication().delegate as AppDelegate
    return appDelegate
  }()
  
  lazy
  var managedObjectContext: NSManagedObjectContext = {
    return self.appDelegate.managedObjectContext!
  }()
  
  private
  func get() -> [Image] {
    let entityDescription =
    NSEntityDescription.entityForName("Image",
      inManagedObjectContext: self.managedObjectContext)
    
    var request =
    NSFetchRequest(entityName: "Image")
    request.returnsObjectsAsFaults = false
    
    var error: NSError?
    
    var images =
    self.managedObjectContext.executeFetchRequest(request, error: &error)! as [Image]
    
    if error != nil {
      println(error)
      return []
    } else if images.count > 0 {
      return images
    } else {
      return []
    }
  }
  
  func exists(md5: String) -> Bool {
    var images = get() as [NSManagedObject]
    if (images as NSArray).count > 0 {
      var exists = false
      for image in images {
        let md5String = image.valueForKey("md5String") as String
        println("md5String: \"\(md5)\", md5: \"\(md5String)\" \(md5 == md5String)")
        if "\(md5)" == "\(md5String)" {
          println("image \(md5)")
          exists = true
        }
      }
      return exists
    }
    return false
  }
  
  func getData() -> NSArray {
    var images = get()
    if images.count > 0 {
      let Images = images as NSArray
      return Images
    } else {
      return []
    }
  }
  
  func clear() {
    var images = get()
    if images.count > 0 {
      // clears every image
      let context: NSManagedObjectContext = self.managedObjectContext
      for image in images {
        context.deleteObject(image as NSManagedObject)
        context.save(nil)
      }
    }
  }
  
  func delete(image: Image, callback: (deleted: Bool) -> Void) {
    var error: NSError?
    self.managedObjectContext.deleteObject(image as NSManagedObject)
    self.managedObjectContext.save(&error)
    callback(deleted: error == nil)
  }
  
  func save(json: AnyObject, representation: NSData) -> (image: Image, error: NSError?) {
    
    let md5 = representation.base64EncodedStringWithOptions(nil).md5

    let newItem = Image.createInManagedObjectContext(self.managedObjectContext,
      date: NSDate(),
      md5String: md5,
      url: json["url"] as String,
      hashImage: json["hash"] as String,
      hashDel: json["hashDel"] as String,
      thumb: json["thu"] as String,
      representation: representation
    )
    
    var error: NSError?
    
    self.managedObjectContext.save(&error)
    
    return (newItem, error)
  }
}
