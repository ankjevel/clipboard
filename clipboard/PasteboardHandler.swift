//
//  PasteboardHandler.swift
//  clipboard
//
//  Created by Dennis Pettersson on 2015-01-31.
//  Copyright (c) 2015 bldr.se. All rights reserved.
//

import Foundation
import Cocoa

class PasteboardHandler:
  NSObject,
  PasteboardObserverSubscriber {
  
  lazy
  var images: Images = {
    return Images()
  }()
  
  lazy
  var appDelegate: AppDelegate = {
    let appDelegate =
    NSApplication.sharedApplication().delegate as AppDelegate
    return appDelegate
  }()
  
  var http = HTTP()
  
  func handleClipboard(data: NSData) {
    if images.exists(data.base64EncodedStringWithOptions(nil).md5) {
      return
    }
    
    func handleResponse(responseData: NSData!, urlResponse: NSURLResponse!, error: NSError!) {
      var jsonErrorOptional: NSError?
      let json: AnyObject! =
        NSJSONSerialization.JSONObjectWithData(responseData,
          options: NSJSONReadingOptions(0),
          error: &jsonErrorOptional)

      if jsonErrorOptional != nil {
        return println("error \(jsonErrorOptional)")
      }
      
      let (image, error) =
        images.save(json, representation: data)
      
      if error == nil {
        appDelegate.writeToPasteboard(image.valueForKey("url") as String)
        NSNotificationCenter.defaultCenter().postNotificationName("load", object: json)
      }
    }
    
    http.request(data, handleCreateResponse: handleResponse)
    
  }
  
  func pasteboardChanged(pasteboard: NSPasteboard) {
    if let png = pasteboard.dataForType("public.png") {
      handleClipboard(png)
//      println("\(_stdlib_getTypeName(png))")
    }
    if let jpg = pasteboard.dataForType("public.jpeg") {
      println("jpg: \(jpg)")
    }
    if let gif = pasteboard.dataForType("com.compuserve.gif") {
      println("gif: \(gif)")
    }
  }
}