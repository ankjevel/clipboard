//
//  ViewController.swift
//  clipboard
//
//  Created by Dennis Pettersson on 2015-01-30.
//  Copyright (c) 2015 bldr.se. All rights reserved.
//

import Cocoa
import AppKit
import Foundation

class ViewController:
  NSViewController,
  NSTableViewDelegate,
  NSTableViewDataSource {
  
  var images: () -> NSArray
  var deleteImage: (Image, (deleted: Bool) -> Void) -> Void
  var http = HTTP()
  
  lazy
  var appDelegate: AppDelegate = {
    let appDelegate =
    NSApplication.sharedApplication().delegate as AppDelegate
    return appDelegate
  }()

  lazy
  var nsc: NSUserNotificationCenter = {
    let nsc = NSUserNotificationCenter.defaultUserNotificationCenter()
    return nsc
  }()
  
  @IBOutlet
  var table: NSTableView!
  
  override
  func viewDidLoad() {
    super.viewDidLoad()
//    Images().clear()
    table.setDelegate(self)
    table.setDataSource(self)
    table.backgroundColor = NSColor.clearColor()
    

    NSNotificationCenter.defaultCenter().addObserver(self, selector: "loadList:", name: "load", object: nil)
  }
  
  override
  func viewDidAppear() {
    super.viewDidAppear()
    self.view.window?.title = "clipboard"
  }
  @IBAction
  func copyURL(sender: NSButton) {
    var image: Image?
    let buttonPosition: CGPoint =
      sender.convertPoint(CGPointZero, toView: self.table)
    let row: Int = table.rowAtPoint(buttonPosition)
      image = (images() as NSArray).objectAtIndex(row) as? Image
    
    if image != nil {
      let imageUnwrapped: Image = image as Image!
      appDelegate.writeToPasteboard(imageUnwrapped.valueForKey("url") as String!)
    }
    
    sender.state = 1
  }

  @IBAction
  func onDelete(sender: NSButton) {
    var image: Image?
    
    func onResponse(responseData: NSData!, urlResponse: NSURLResponse!, error: NSError!) {
      println("responseData: \(responseData), urlResponse: \(urlResponse), error: \(error)")
      if error != nil { return }
      deleteImage(image!) {
        if $0 == true {
          dispatch_async(dispatch_get_main_queue(), {
            self.table.reloadData()
          })
        }
      }
    }

    let buttonPosition: CGPoint =
      sender.convertPoint(CGPointZero, toView: self.table)
    let row: Int = table.rowAtPoint(buttonPosition)
    image = (images() as NSArray).objectAtIndex(row) as? Image
    
    if image != nil {
      let imageUnwrapped: Image = image as Image!
      let imageHash = imageUnwrapped.valueForKey("hashImage") as String
      let delHash = imageUnwrapped.valueForKey("hashDel") as String
      http.request(imageHash, delHash: delHash, handleDeleteResponse: onResponse)
    }
    
    sender.state = 1
  }
  
  func loadList(notification: NSNotification) {
    dispatch_async(dispatch_get_main_queue(), {
      self.table.reloadData()
      println(notification)
    
      if notification.object?.count > 0 && notification.object?.valueForKey("url") != nil {
        var displayNote:NSUserNotification = NSUserNotification()
        displayNote.title = "updated clipboard"
        displayNote.subtitle = notification.object!.valueForKey("url") as String!
        self.nsc.scheduleNotification(displayNote)
      }
    })
  }
  
  override
  var representedObject: AnyObject? {
    didSet {
    }
  }
  
  required
  init?(coder aDecoder: NSCoder) {
    let imgs = Images()
    images = imgs.getData
    deleteImage = imgs.delete
    super.init(coder: aDecoder)
  }
  
  func numberOfRowsInTableView(aTableView: NSTableView!) -> Int {
    let numberOfRows: Int = images().count
    return numberOfRows
  }
  
  func tableView(tableView: NSTableView!, viewForTableColumn: NSTableColumn!, row: Int) -> AnyObject! {
    let tableIdentifier = viewForTableColumn.identifier as String
    
    if let result = tableView.makeViewWithIdentifier(tableIdentifier, owner: self) as? NSTableCellView {
      var newString: String = ""
      var imageView: NSImageView?
      if let image = (images() as NSArray).objectAtIndex(row) as? Image {
        if tableIdentifier == "representation" {
          if let representation = image.valueForKey(tableIdentifier) as? NSData {
            imageView =
              NSImageView(frame: NSRect(
                x: 5,
                y: 0,
                width: (result.frame.width - 20),
                height: (result.frame.height - 10)
              ))
            
            imageView!.image =
              NSImage(data: representation)
          }
        } else {
          if let identifier = image.valueForKey(tableIdentifier) as? String {
            newString = identifier
          }
        }
      }
      switch tableIdentifier {
      case "url":
        viewForTableColumn.title = ""
        break
      case "md5String":
        viewForTableColumn.title = "MD5"
        break
      case "representation":
        viewForTableColumn.title = "Image"
        break
      case "hashDel":
        viewForTableColumn.title = ""
        break
      default:
        viewForTableColumn.title = tableIdentifier
      }
      
      result.textField?.stringValue = newString

      if tableIdentifier == "representation" {
        result.addSubview(imageView!)
      }
      return result
    }
    
    return nil
  }

}

