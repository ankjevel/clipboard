//
//  Image.swift
//  clipboard
//
//  Created by Dennis Pettersson on 2015-02-04.
//  Copyright (c) 2015 bldr.se. All rights reserved.
//

import Foundation
import CoreData

class Image: NSManagedObject {
  
  let version: Float = 1.3
  @NSManaged var date: NSDate
  @NSManaged var md5String: String
  @NSManaged var url: String
  @NSManaged var hashImage: String
  @NSManaged var hashDel: String
  @NSManaged var thumb: String
  @NSManaged var representation: NSData
  
  class func createInManagedObjectContext(
    moc: NSManagedObjectContext,
    date: NSDate,
    md5String: String,
    url: String,
    hashImage: String,
    hashDel: String,
    thumb: String,
    representation: NSData
  ) -> Image {
    let image =
      NSEntityDescription.insertNewObjectForEntityForName("Image", inManagedObjectContext: moc) as Image
    
    image.date = date
    image.md5String = md5String
    image.url = url
    image.hashImage = hashImage
    image.hashDel = hashDel
    image.thumb = thumb
    image.representation = representation
    
    return image
  }
  
}
